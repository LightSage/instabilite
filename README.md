## instabilité

Script that gets news from TX's main page and sends it through a webhook.

## Screenshot

![](https://cdn.discordapp.com/attachments/527965708793937960/591664376591810590/unknown.png)

## Setup

- Install Python3.6+
- Install the packages in requirements.txt
- Configure `config.py`
- Run the script, and run it periodically with systemd timers, cron, etc.
