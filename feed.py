import feedparser
import requests
import config
import time
import json
import os

data = json.load(open("data.json", "r"))
feed = feedparser.parse('http://team-xecuter.com/feed')

for entry in feed["entries"]:
    # Grab link and description
    title = entry["title"]
    link = entry["link"]
    # :whenlifegetsatyou:
    description = entry["description"].replace("[&#8230;]", "[...]")

    pub_time = time.mktime(entry["published_parsed"])
    if pub_time <= data["lastupdate"]:
        continue
    
    webhook_msg = f"🚨 **New Post:** {title}\n"\
                  f"__Description:__ {description}"
    if config.link is True:
        webhook_msg += f"\nMore details at <{link}>"

    data["lastupdate"] = pub_time

    print(webhook_msg)

    for hook in config.webhooks:
        requests.post(hook, data={"content": webhook_msg})

if os.path.isfile("data.json"):
    with open("data.json", "w") as f:
        json.dump(data, f)
else:
    print("This isn\'t properly setup!")
